Ext.define("venkys_sale.view.AllcollContainer",{
	extend: "Ext.Panel",
	xtype: "allcollcontainer",
    requires: ["venkys_sale.view.AllcollPanel"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Plants",
			id: 'allcollectionid',
			
			
			items: [
			   {
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'allcollBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "Refresh", 
					action: 'refreshallcolltap',
					//iconCls: 'reply',
                   // iconMask: true,
				},
				
			
				
			]
		};
		
		var footerbar = {
			xtype: "toolbar",
			docked: "bottom",
			//title: "Plants",
			//id: 'allcollectionid',
			
			
			items: [
			   {
					xtype: "button",
					text: "index", 
					action: 'blistBack',
				},
			
				{ xtype: "spacer" },
				
			
				
			]
		};

		this.add([toolbar,footerbar, { xtype: "allcollpanel"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Plants", 
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});