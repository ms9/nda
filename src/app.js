//<debug>
Ext.Loader.setPath({
    'Ext.ux.touch.grid': 'sdk/Ext.ux.touch.grid',
	'Ext': 'sdk/src'
});
//</debug>
/*Ext.Loader.setConfig({
    enabled : true,
    paths   : {
		'Ext': 'sdk/src'
    }
});*/
/*Ext.Loader.setConfig({
    enabled : true,
    paths   : {
        'Ext.ux.touch.grid': 'sdk/Ext.ux.touch.grid',
		'Ext': 'sdk/src'
    }
});*/
Ext.application({
    name: 'venkys_sale',

    requires: [
        'Ext.MessageBox'
    ],

    controllers: ["Feed"],
    //models: ["Plant","Plantname","All","SaleDetails","Dispatch","Collection","Division","DivisionName","Cattle","Hatchery","Integration","Biscuit","Cattlefeed","HatcheryList","IntegrationList","BiscuitList"],
    
    //views: ["Viewport","MainPanel","Plantpanel","PlantpanelContainer",/*"SaleList","SaleListContainer","SaleDetail",*/"SaleDetails","SaleDetailsContainer",/*"Dispatch","DispatchContainer","Collection","CollectionContainer","Collector","CollectorPanel",*/"DivisionPanel","DivisionContainer","CattlePanel","CattleContainer","HatcheryPanel","HatcheryContainer","IntegrationPanel","IntegrationContainer","BiscuitPanel","BiscuitContainer","CattleFeed","CattlefeedContainer",/*"Contact","ContactPanel",*/"HatcheryList","HatcherylistContainer","IntegrationList","IntegrationlistPanel","BiscuitList","BiscuitlistContainer",/*"OverseasPanel","OverseaspanelContainer","OverseasDetails","OverseasDetailsContainer",*/"AllPanel","AllContainer","AllsaleContainer","AllsalePanel","AlldispatchPanel","AlldispatchContainer","AllcollContainer","AllcollPanel","LiveSale","LivesaleContainer","LiveDispatch","LivedispatchContainer","AllpayPanel","AllpayContainer"],
    models: ["Plant","Plantname","Division","DivisionName"],//"SaleDetails","Dispatch","Collection","Division","DivisionName","Cattle","Hatchery","Integration","Biscuit","Cattlefeed","HatcheryList","IntegrationList","BiscuitList","Overseas","OverseasDetails","Poultry","Salesdispatch","Coll_pay","All_pay","Live_sale","Live_dispatch"],
    stores: ["Plant","Plantname","All","SaleDetails",/*"Dispatch","Collection",*/"Division","DivisionName","Cattle","Hatchery","Integration","Biscuit","Cattlefeed","HatcheryList","IntegrationList","BiscuitList",/*"Overseas","OverseasDetails","Poultry",*/"Salesdispatch","Coll_pay","All_pay","Live_sale","Live_dispatch"],
	views: ["Viewport","MainPanel","Plantpanel","PlantpanelContainer",/*"SaleList","SaleListContainer","SaleDetail",*/"SaleDetails","SaleDetailsContainer",/*"Dispatch","DispatchContainer","Collection","CollectionContainer","Collector","CollectorPanel",*/"DivisionPanel","DivisionContainer","CattlePanel","CattleContainer","HatcheryPanel","HatcheryContainer","IntegrationPanel","IntegrationContainer","BiscuitPanel","BiscuitContainer","CattleFeed","CattlefeedContainer",/*"Contact","ContactPanel",*/"HatcheryList","HatcherylistContainer","IntegrationList","IntegrationlistPanel","BiscuitList","BiscuitlistContainer",/*"OverseasPanel","OverseaspanelContainer","OverseasDetails","OverseasDetailsContainer",*/"AllPanel","AllContainer","AllsaleContainer","AllsalePanel","AlldispatchPanel","AlldispatchContainer","AllcollContainer","AllcollPanel","LiveSale","LivesaleContainer","LiveDispatch","LivedispatchContainer","AllpayPanel","AllpayContainer"],
    icon: {
        57: 'resources/icons/Icon.png',
        72: 'resources/icons/Icon~ipad.png',
        114: 'resources/icons/Icon@2x.png',
        144: 'resources/icons/Icon~ipad@2x.png'
    },
    
    phoneStartupScreen: 'resources/loading/Homescreen.jpg',
    tabletStartupScreen: 'resources/loading/Homescreen~ipad.jpg',

    launch: function() {
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();

        // Initialize the main view
        Ext.Viewport.add(Ext.create('venkys_sale.view.Viewport'));

		Ext.Viewport.add(Ext.create('Sencha Tocuh MVC application'));
		

		Ext.Viewport.add(Ext.create('venkys_sale'));

    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function() {
                window.location.reload();
				window.location.reload( version 2.0);
            }
        );
    }
});
